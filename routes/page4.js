/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
// -----------

// ---- 기본 library 셋팅
global.__SPRING_SERVICE = 'http://recommendation-service.169.56.170.165.nip.io';
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
// --------------

// --- page화면 표시
router.post("/page4", (req, res) => {
	util.log("page4 들어왔어??");
	axios.post(__SPRING_SERVICE+"/page4",
			{
				userId:req.body.userId,
				prodCode:req.body.prodCode
			}
		)
		.then((ret) => {
			if(ret.status == 200) {
				util.log("page4 성공???");
				util.log(ret.data);
				res.render("views/page4", {postData: ret.data});
			} else {
				res.json({
					resText: "!200"
				});
			}
		})
		.catch((error) => {
			util.log("page4 실패???");
			console.error(error);
			res.json({
				resText: error
			});
		});	
	
});
// --------------


module.exports = router;