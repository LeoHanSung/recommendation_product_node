/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
// -----------

// ---- 기본 library 셋팅
const __SPRING_SERVICE = 'http://recommendation-service.169.56.170.165.nip.io';
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
// --------------

// --- Login화면 표시
router.get("/login", (req, res) => {
	util.log("Login 화면");
	res.render("views/login");
});
// --------------

// --- 인증처리 로직
router.post("/login", (req, res) => {
	util.log("Login Process");
	util.log("__SPRING_SERVICE : "+__SPRING_SERVICE);

	
	axios.post(__SPRING_SERVICE+"/userInfo",
		{
			userId:req.body.userId
		}
	)
	.then((ret) => {
		if(ret.status == 200) {
			console.log(ret);
		    sess = req.session;
		    sess.userId = req.body.userId;
		    sess.userInfo = ret.data;
		    
			res.render("views/page1", {postData: ret.data});
					
		} else {
			res.json({
				resText: "!200"
			});
		}
	})
	.catch((error) => {
		console.error(error);
		res.json({
			resText: error
		});
	});	
	


});
// ----------------


// --- page2화면 표시
router.post("/page2", (req, res) => {
	util.log("page2 화면");

    sess = req.session;

	
	res.render("views/page2",{postData: sess.userInfo});
	

});
// --------------


module.exports = router;