/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
// -----------

// ---- 기본 library 셋팅
global.__SPRING_SERVICE = 'http://recommendation-service.169.56.170.165.nip.io';
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
// --------------

// --- page3 데이터분석 api로 가면 된다.
router.get("/page3", (req, res) => {
	util.log("page3 들어왔어??");
	sess = req.session;
	
	axios.post(__SPRING_SERVICE+"/page3",
			{
				userId:req.query.userId
			}
		)
		.then((ret) => {
			if(ret.status == 200) {
				util.log("page3->ajax->갔다 와서 뿌려주는 곳?");
				util.log(ret.data);
			    sess = req.session;
			    res.json({
			    	userInfo: sess.userInfo,
			    	postData: ret.data
				});
			    						
			} else {
				res.json({
					resText: "!200"
				});
			}
		})
		.catch((error) => {
			console.error(error);
			res.json({
				resText: error
			});
		});	
});

module.exports = router;